package lab;

import wash.WashingIO;

abstract public class AbstractWashingProgram extends MessagingThread<WashingMessage> {

    protected WashingIO io;
    protected MessagingThread<WashingMessage> temp;
    protected MessagingThread<WashingMessage> water;
    protected MessagingThread<WashingMessage> spin;
    protected int programNumber;

    public AbstractWashingProgram(WashingIO io,
                                  MessagingThread<WashingMessage> temp,
                                  MessagingThread<WashingMessage> water,
                                  MessagingThread<WashingMessage> spin) {
        this.io = io;
        this.temp = temp;
        this.water = water;
        this.spin = spin;
    }

    abstract protected void doWashStuff() throws InterruptedException;

    public void run() {
        try {
            System.out.println("washing program " + programNumber + " started.");
            io.lock(true);
            sendWashMessage(water, WashingMessage.WATER_FILL, 10, "");
            receive();

            doWashStuff();

            sendWashMessage(water, WashingMessage.WATER_DRAIN, 0, "");
            receive();
            io.lock(false);
            System.out.println("washing program " + programNumber + " done.");
        } catch (InterruptedException e) {
            temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));
            water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
            spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
            System.out.println("washing program terminated");
        }
    }

    protected void sendWashMessage(MessagingThread consumer, int code, double value, String print) {
        consumer.send(new WashingMessage(this, code, value));
        if (!print.isEmpty()) {
            System.out.println("-- " + print);
        }
    }
}
