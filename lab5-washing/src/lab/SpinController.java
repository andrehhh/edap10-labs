package lab;

import wash.WashingIO;

public class SpinController extends MessagingThread<WashingMessage> {

    private WashingIO io;

    /*
        1 for clockwise.
        0 for stop.
        -1 for anti-clockwise.
     */
    private int spinning;
    private boolean centrifuge;

    public SpinController(WashingIO io) {
        this.io = io;
        this.spinning = 0;
    }

    @Override
    public void run() {
        try {

            // ... TODO ...

            while (true) {
                // wait for up to a (simulated) minute for a WashingMessage
                WashingMessage m = receiveWithTimeout(60000 / Wash.SPEEDUP);

                // if m is null, it means a minute passed and no message was received
                if (m != null) {
                    System.out.println("got " + m);

                    switch(m.getCommand()) {
                        case WashingMessage.SPIN_OFF:
                            io.setSpinMode(WashingIO.SPIN_IDLE);
                            centrifuge = false;
                            spinning = 0;
                            break;
                        case WashingMessage.SPIN_SLOW:
                            spinning = 1;
                            centrifuge = false;
                            break;
                        case WashingMessage.SPIN_FAST:
                            io.setSpinMode(WashingIO.SPIN_FAST);
                            centrifuge = true;
                            break;
                    }
                }
                handleSpin();
            }
        } catch (InterruptedException unexpected) {
            // we don't expect this thread to be interrupted,
            // so throw an error if it happens anyway
            throw new Error(unexpected);
        }
    }

    private void handleSpin() {
        // change direction of spin.
        if (!centrifuge && spinning != 0) {
            spinning *= -1;

            switch (spinning) {
                case 1:
                    io.setSpinMode(WashingIO.SPIN_RIGHT);
                    break;
                case -1:
                    io.setSpinMode(WashingIO.SPIN_LEFT);
                    break;
                default:
                    io.setSpinMode(WashingIO.SPIN_IDLE);
            }
        }
    }
}
