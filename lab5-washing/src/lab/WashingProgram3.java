package lab;

import wash.WashingIO;

class WashingProgram3 extends AbstractWashingProgram{

    public WashingProgram3(WashingIO io,
                           MessagingThread<WashingMessage> temp,
                           MessagingThread<WashingMessage> water,
                           MessagingThread<WashingMessage> spin) {
        super(io, temp, water, spin);
        this.programNumber = 3;
    }

    @Override
    public void run() {
        System.out.println("washing program 3 started");
        // Override as AbstractWashingProgram.run()  locks/fills and drains/empties.
        doWashStuff();
        System.out.println("washing program 3 finished");
    }

    @Override
    protected void doWashStuff() {
        try {

            // Switch off heating
            temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));

            // Switch off spin
            spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));

            // Drain barrel (may take some time)
            water.send(new WashingMessage(this, WashingMessage.WATER_DRAIN));
            WashingMessage ack = receive();  // wait for acknowledgment
            System.out.println("got " + ack);
            water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));

            // Unlock hatch
            io.lock(false);
        } catch (InterruptedException e) {
            temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));
            water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
            spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
            System.out.println("washing program terminated");
        }
    }
}
