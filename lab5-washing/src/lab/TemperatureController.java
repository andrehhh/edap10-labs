package lab;

import wash.WashingIO;

public class TemperatureController extends MessagingThread<WashingMessage> {

    private final double mu = 0.0478 * 10, ml = 0.000952 * 10, moe = 0.2;
    private double goalTemp = 0;
    private boolean heatOn = false;

    private WashingIO io;

    public TemperatureController(WashingIO io) {
        this.io = io;
    }

    @Override
    public void run() {
        try {
            while (true) {
                var m = receiveWithTimeout(10000 / Wash.SPEEDUP); // wait max 10 sec.

                if (m != null) {
                    goalTemp = m.getValue();

                    switch (m.getCommand()) {
                        case WashingMessage.TEMP_SET:
                            while ( io.getWaterLevel() > 0 && io.getTemperature() < goalTemp-mu ) { // handle initial heating.
                                io.heat(true); // TODO:fix busy "wait"
                            }
                            io.heat(false);
                            m.getSender().send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
                            break;
                        case WashingMessage.TEMP_IDLE:
                            io.heat(false);
                            break;
                    }
                }

                // Keeps the _T_ instide [_N_-2, _N_)
                if (io.getTemperature() - ml - moe <= goalTemp-2 ) {
                    heatOn = true;
                }
                if (io.getTemperature() + mu + moe> goalTemp) {
                    heatOn = false;
                }

                io.heat(heatOn);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
