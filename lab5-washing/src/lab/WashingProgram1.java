package lab;

import wash.WashingIO;

/**
 * Program 3 for washing machine.
 * Serves as an example of how washing programs are structured.
 * 
 * This short program stops all regulation of temperature and water
 * levels, stops the barrel from spinning, and drains the machine
 * of water.
 * 
 * It is can be used after an emergency stop (program 0) or a
 * power failure.
 */
class WashingProgram1 extends AbstractWashingProgram {

    protected double washTemp = 40;

    public WashingProgram1(WashingIO io,
                           MessagingThread<WashingMessage> temp,
                           MessagingThread<WashingMessage> water,
                           MessagingThread<WashingMessage> spin) {
        super(io, temp, water, spin);
        this.programNumber = 1;
    }

    @Override
    protected void doWashStuff() throws InterruptedException {
        // Heat to 40 degrees.
        sendWashMessage(temp, WashingMessage.TEMP_SET, washTemp, "");
        receive();

        // spin for 30 minutes (one minute == 60000 milliseconds)
        sendWashMessage(spin, WashingMessage.SPIN_SLOW, 0, "");
        Thread.sleep(30 * 60000 / Wash.SPEEDUP);

        // Wait for the machine to drain.
        sendWashMessage(spin, WashingMessage.SPIN_OFF, 0, "");
        sendWashMessage(temp, WashingMessage.TEMP_IDLE, 0, "");
        sendWashMessage(water, WashingMessage.WATER_DRAIN, 0, "");
        receive();

        // Rinse 5 times.
        for (int i = 0; i < 5; i++) {
            sendWashMessage(water, WashingMessage.WATER_FILL, 10, "");
            receive();
            sendWashMessage(water, WashingMessage.WATER_DRAIN, 0, "");
            receive();
        }

        // Centrifuge for 5 min
        sendWashMessage(spin, WashingMessage.SPIN_FAST, 0, "");
        Thread.sleep(5 * 60000 / Wash.SPEEDUP);

        // instruct SpinController to stop spin barrel spin
        sendWashMessage(spin, WashingMessage.SPIN_OFF, 0, "");
    }
}
