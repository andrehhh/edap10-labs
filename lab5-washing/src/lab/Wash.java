package lab;
import simulator.WashingSimulator;
import wash.WashingIO;

public class Wash {

    // simulation speed-up factor:
    // 50 means the simulation is 50 times faster than real time
    public static final int SPEEDUP = 200;

    public static void main(String[] args) throws InterruptedException {
        WashingSimulator sim = new WashingSimulator(SPEEDUP);
        
        WashingIO io = sim.startSimulation();

        TemperatureController temp = new TemperatureController(io);
        WaterController water = new WaterController(io);
        SpinController spin = new SpinController(io);

        temp.start();
        water.start();
        spin.start();

        WashingProgram1 wp1 = new WashingProgram1(io, temp, water, spin);
        WashingProgram2 wp2 = new WashingProgram2(io, temp, water, spin);
        WashingProgram3 wp3 = new WashingProgram3(io, temp, water, spin);

        while (true) {
            int n = io.awaitButton();
            System.out.println("user selected program " + n);

            switch (n) {
                case 0:
                    StopProgram sp = new StopProgram(io, temp, water, spin);
                    wp1.interrupt();
                    wp2.interrupt();
                    wp3.interrupt();

                    sp.start();
                    sp.join();
                    break;
                case 1:
                    try {
                        wp1.start();
                    } catch (IllegalThreadStateException e) {
                        wp1 = new WashingProgram1(io, temp, water, spin);
                        wp1.start();
                    }
                    break;
                case 2:
                    try {
                        wp2.start();
                    } catch (IllegalThreadStateException e) {
                        wp2 = new WashingProgram2(io, temp, water, spin);
                        wp2.start();
                    }
                    break;
                case 3:
                    try {
                        wp3.start();
                    } catch (IllegalThreadStateException e) {
                        wp3 = new WashingProgram3(io, temp, water, spin);
                        wp3.start();
                    }
                    break;
            }
        }
    }
};
