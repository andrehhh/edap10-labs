package lab;

import wash.WashingIO;

public class WaterController extends MessagingThread<WashingMessage> {

    private WashingIO io;

    public WaterController(WashingIO io) {
        this.io = io;
    }

    @Override
    public void run() {
        try {
            while (true) {
                var msg = receiveWithTimeout(1000 / Wash.SPEEDUP); // once per sec.

                if (msg != null) {
                    switch (msg.getCommand()) {
                        case WashingMessage.WATER_FILL:
                            if (io.getWaterLevel() < msg.getValue()) { // guard against overflow
                                io.fill(true);
                                Thread.sleep((long) msg.getValue() * 10 * 1000 / Wash.SPEEDUP);
                                io.fill(false);
                            }
                            msg.getSender().send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
                            break;
                        case WashingMessage.WATER_DRAIN:
                            io.fill(false); // to make this waterproof. höhö
                            io.drain(true);
                            while (io.getWaterLevel() > 0); // TODO fix busy-wait?
                            io.drain(false);
                            msg.getSender().send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
                            break;
                        case WashingMessage.WATER_IDLE:
                            io.drain(false);
                            io.fill(false);
                            break;
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
