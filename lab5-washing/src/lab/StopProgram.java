package lab;

import com.sun.jdi.request.WatchpointRequest;
import wash.WashingIO;

class StopProgram extends MessagingThread<WashingMessage> {

    private WashingIO io;
    private MessagingThread<WashingMessage> temp;
    private MessagingThread<WashingMessage> water;
    private MessagingThread<WashingMessage> spin;

    public StopProgram(WashingIO io,
                       MessagingThread<WashingMessage> temp,
                       MessagingThread<WashingMessage> water,
                       MessagingThread<WashingMessage> spin) {
        this.io = io;
        this.temp = temp;
        this.water = water;
        this.spin = spin;
    }
    
    @Override
    public void run() {
        spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
        temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));
        water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
    }
}
