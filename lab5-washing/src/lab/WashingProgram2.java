package lab;

import wash.WashingIO;

/**
 * Program 3 for washing machine.
 * Serves as an example of how washing programs are structured.
 * 
 * This short program stops all regulation of temperature and water
 * levels, stops the barrel from spinning, and drains the machine
 * of water.
 * 
 * It is can be used after an emergency stop (program 0) or a
 * power failure.
 */
class WashingProgram2 extends WashingProgram1 {

    public WashingProgram2(WashingIO io,
                           MessagingThread<WashingMessage> temp,
                           MessagingThread<WashingMessage> water,
                           MessagingThread<WashingMessage> spin) {
        super(io, temp, water, spin);
        this.programNumber = 2;
    }

    @Override
    protected void doWashStuff() throws InterruptedException{
        // Heat to 40 degrees.
        sendWashMessage(temp, WashingMessage.TEMP_SET, washTemp, "");
        receive();

        // Pre-wash for 15 min.
        sendWashMessage(spin, WashingMessage.SPIN_SLOW, 0, "");
        Thread.sleep(15 * 60000 / Wash.SPEEDUP);

        // Change water temp to 60 deg. (Is used in WashingProgram1::doWashStuff().)
        washTemp = 60;

        super.doWashStuff();
    }
}
