

import java.math.BigInteger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import client.view.ButtonCallback;
import client.view.ProgressItem;
import client.view.StatusWindow;
import client.view.WorklistItem;
import network.Sniffer;
import network.SnifferCallback;
import rsa.Factorizer;
import rsa.ProgressTracker;

public class CodeBreaker implements SnifferCallback, ButtonCallback, ProgressTracker {

    private final JPanel workList;
    private final JPanel progressList;
    
    private final JProgressBar mainProgressBar;
    private int mainCurrentProgress;
    private int mainPrevProgress;

    private ExecutorService background;

    // -----------------------------------------------------------------------
    
    private CodeBreaker() {
        StatusWindow w  = new StatusWindow();

        workList        = w.getWorkList();
        progressList    = w.getProgressList();
        mainProgressBar = w.getProgressBar();
        w.enableErrorChecks();
        background = Executors.newFixedThreadPool(2);

        new Sniffer(this).start();

    }
    
    // -----------------------------------------------------------------------
    
    public static void main(String[] args) throws Exception {

        /*
         * Most Swing operations (such as creating view elements) must be
         * performed in the Swing EDT (Event Dispatch Thread).
         * 
         * That's what SwingUtilities.invokeLater is for.
         */

        SwingUtilities.invokeLater(() -> new CodeBreaker());
    }

    // -----------------------------------------------------------------------

    /** Called by a Sniffer thread when an encrypted message is obtained. */
    @Override
    public void onMessageIntercepted(String message, BigInteger n) {
        SwingUtilities.invokeLater( () -> workList.add(new WorklistItem(n, message, this)) );
    }

    public synchronized void changeMainProgressValue(int delta) {
        mainCurrentProgress += delta;
        mainProgressBar.setValue(mainCurrentProgress);
    }

    /* ==================== ButtonCallback ==================== */
    /**
     * Responsible for transferring the WorklistItem to the progress-column.
     * @param promoteToProgressItem
     */
    @Override
    public void onBreakClicked(WorklistItem promoteToProgressItem) {
        var currMax = mainProgressBar.getMaximum();
        mainProgressBar.setMaximum(currMax + 1000000);

        var n = promoteToProgressItem.getN();
        var code = promoteToProgressItem.getCode();
        var pi = new ProgressItem(n, code, this);
        var tracker = new ProgressTrackerImpl(pi, this);

        SwingUtilities.invokeLater( () -> {
            workList.remove(promoteToProgressItem);
            progressList.add(pi);
        });

        background.execute( () -> {
            // Make sure the computation is done in the background
            var cracked = Factorizer.crack(code, n, tracker);
            // Update in GUI.
            SwingUtilities.invokeLater( () -> pi.getTextArea().setText(cracked));
        });
    }

    @Override
    public void onRemoveClicked(ProgressItem toBeRemoved) {
        SwingUtilities.invokeLater( () -> {
            changeMainProgressValue(-1000000);

            // this wont happen that often.
            var currMax = mainProgressBar.getMaximum();
            mainProgressBar.setMaximum(currMax - 1000000);

            progressList.remove(toBeRemoved);
        });

    }

    /* ==================== ProgressTracker ==================== */
    @Override
    public void onProgress(int ppmDelta) {
        SwingUtilities.invokeLater( () -> changeMainProgressValue(ppmDelta));
    }
}
