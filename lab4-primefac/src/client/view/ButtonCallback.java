package client.view;

import client.view.WorklistItem;

public interface ButtonCallback {
    void onBreakClicked(WorklistItem promoteToProgressItem);
    void onRemoveClicked(ProgressItem toBeRemoved);
}
