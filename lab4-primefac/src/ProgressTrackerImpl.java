import client.view.ProgressItem;
import rsa.ProgressTracker;

import javax.swing.*;

/**
 * Implementation of ProgressTracker that is responsible for updating the ProgressBars of a ProgressItem.
 */
public class ProgressTrackerImpl implements ProgressTracker {
    public static final int PROGRESS_MAX = 1000000;
    private int currProgress = 0;
    private int prevProgress = -1;
    private ProgressItem pi;
    private JProgressBar progressBar;
    private ProgressTracker globalTracker;

    public ProgressTrackerImpl(ProgressItem pi, ProgressTracker globalTracker) {
        this.pi = pi;
        this.progressBar = pi.getProgressBar();
        this.globalTracker = globalTracker;
    }

    @Override
    public void onProgress(int ppmDelta) {
        globalTracker.onProgress(ppmDelta); // forward the update.

        prevProgress = currProgress;
        currProgress += ppmDelta;
        if (currProgress == PROGRESS_MAX) {
            pi.done();
        }
        if (currProgress/10000 != prevProgress/10000) { // only update when there is a difference in procent
            SwingUtilities.invokeLater( () -> progressBar.setValue(currProgress) );
        }
    }
}
