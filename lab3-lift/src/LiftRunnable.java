import lift.LiftView;

public class LiftRunnable implements Runnable{
    LiftView view;
    LiftMonitor lf;

    public LiftRunnable(LiftView view, LiftMonitor lf) {
        this.view = view;
        this.lf = lf;
    }

    @Override
    public void run() {
        while (true) {
            for (int i = 0; i < 6; i++) {
                view.moveLift(i, i + 1);
                lf.setPosition(i+1);
                try {
                    waitOutside(2000);
                    while (lf.isDudeBoarding()) {
                        waitOutside(2000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            for (int j = 6; j > 0; j--) {
                view.moveLift(j, j - 1);
                lf.setPosition(j-1);
                try {
                    waitOutside(2000);
                    while (lf.isDudeBoarding()) {
                        waitOutside(2000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /** Helper method: sleep outside of monitor for ’millis’ milliseconds. */
    private void waitOutside(long millis) throws InterruptedException {
        long timeToWakeUp = System.currentTimeMillis() + millis;
        while (System.currentTimeMillis() < timeToWakeUp) {
            long dt = timeToWakeUp - System.currentTimeMillis();
            wait(dt);
        }
    }

}
