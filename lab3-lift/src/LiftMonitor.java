public class LiftMonitor {
    private int passengers = 0;
    private int position = 0;
    private int[] entryWait = new int[6];
    private int[] exitWait = new int[6];
    private boolean dudeIsBoarding = false;

    /* Setters/Getters */
    public synchronized void changePassengers(int delta) {
        passengers += delta;
    }

    public synchronized int getPosition() {
        return position;
    }

    public synchronized void setPosition(int position) {
        this.position = position;
    }

    public synchronized int getEntryWait(int index) {
        return entryWait[index];
    }

    public synchronized void setEntryWait(int entryWait, int index) {
        this.entryWait[index] = entryWait;
    }

    public synchronized int getExitWait(int index) {
        return exitWait[index];
    }

    public synchronized void setExitWait(int exitWait, int index) {
        this.exitWait[index] = exitWait;
    }

    public synchronized boolean isDudeBoarding() {
        return dudeIsBoarding;
    }

    public synchronized void setDudeBoarding(boolean dudeIsBoarding) {
        this.dudeIsBoarding = dudeIsBoarding;
    }
}

