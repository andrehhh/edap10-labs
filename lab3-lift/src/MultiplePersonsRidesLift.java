
import lift.LiftView;
import lift.Passenger;

public class MultiplePersonsRidesLift {

    // Method to escape the static context. Gets called by main.
    public void doThings() {

        LiftView view = new LiftView();
        LiftMonitor lf = new LiftMonitor();

        new Thread(new LiftRunnable(view, lf), "lift").start();

        Passenger passenger = view.createPassenger();

        new Thread(() -> {
            passenger.begin();
            notifyAll();
            while(lf.getPosition() != passenger.getStartFloor()) { /* TODO fixa busy wait*/ }
            lf.setDudeBoarding(true);
            passenger.enterLift();
            lf.setDudeBoarding(false);
            notifyAll();
            while(lf.getPosition() != passenger.getDestinationFloor()) { /* TODO fixa busy wait*/ }
            passenger.exitLift();
            passenger.end();
        }, "passenger").start();
    }

    public static void main(String[] args) {
        new MultiplePersonsRidesLift().doThings();
    }
}
