package factory.controller;

import factory.model.DigitalSignal;
import factory.model.SignalValue;
import factory.model.WidgetKind;
import factory.swingview.Factory;

public class ToolController {
    private final DigitalSignal conveyor, press, paint;
    private final long pressingMillis, paintingMillis;
    
    public ToolController(DigitalSignal conveyor,
                          DigitalSignal press,
                          DigitalSignal paint,
                          long pressingMillis,
                          long paintingMillis)
    {
        this.conveyor = conveyor;
        this.press = press;
        this.paint = paint;
        this.pressingMillis = pressingMillis;
        this.paintingMillis = paintingMillis;
    }

    private boolean isPaintingRN = false;
    private boolean isPressingRN = false;

    private synchronized void startConveyor() {
        if (! (isPaintingRN && isPressingRN)) {
            conveyor.on();
        }
    }

    private synchronized void stopConveyor() {
        conveyor.off();
    }


    /** Helper method: sleep outside of monitor for ’millis’ milliseconds. */
    private void waitOutside(long millis) throws InterruptedException {
        long timeToWakeUp = System.currentTimeMillis() + millis;
        while (System.currentTimeMillis() < timeToWakeUp) {
            long dt = timeToWakeUp - System.currentTimeMillis();
            wait(dt);
        }
    }
    public synchronized void onPressSensorHigh(WidgetKind widgetKind) throws InterruptedException {
        //
        // TODO: you will need to modify this method
        //
        if (widgetKind == WidgetKind.BLUE_RECTANGULAR_WIDGET) {
            isPressingRN = true;
            stopConveyor();
            press.on();
            waitOutside(pressingMillis);
            press.off();
            waitOutside(pressingMillis);   // press needs this time to retract
            startConveyor();
            isPressingRN = false;
        }
    }

    // Called through callbacks
    public synchronized void onPaintSensorHigh(WidgetKind widgetKind) throws InterruptedException {
        if (widgetKind == WidgetKind.ORANGE_ROUND_WIDGET) {
            isPaintingRN = true;
            stopConveyor();
        	paint.on();
        	waitOutside(paintingMillis);
        	paint.off();
            startConveyor();
            isPaintingRN = false;
        }
    }
    
    // -----------------------------------------------------------------------
    
    public static void main(String[] args) {
        Factory factory = new Factory();
        factory.startSimulation();
    }
}
