import java.util.StringJoiner;
import java.util.concurrent.Semaphore;

public class Data {
    private int time;
    private int alarmTime;
    private boolean alarmActive;
    private boolean alarmShouldShutUp;
    private Semaphore mutex;

    public Data(int time, Semaphore mutex) {
        this.time = time;
        this.alarmTime = 0;
        this.alarmActive = false;
        this.alarmShouldShutUp = false;
        this.mutex = mutex;
    }
// ------ CLOCK TIME ------
    public int getTime() throws InterruptedException {
        mutex.acquire();
        var tajm = this.time;
        mutex.release();
        return tajm;
    }

    public void setTime(int time) throws InterruptedException {
        mutex.acquire();
        this.time = time;
        mutex.release();
        sanitizeClock();
    }

    /**
     * Increments the current time by delta.
     * @param delta the amount added on the current time.
     * @return the new time!
     * @throws InterruptedException
     */
    public int incrementTime(int delta) throws InterruptedException {
        var newTime = getTime()+delta;
        setTime(newTime);
        return newTime;
    }

    /**
     * Used to be able to carelessly be able to add some seconds to the clock. And this method cleans up the mess
     * afterwards.
     * @throws InterruptedException
     */
    private void sanitizeClock() throws InterruptedException {
        var currSec = getCurrentSec();
        var currMin = getCurrentMin();
        var currHour = getCurrentHour();
        //System.out.println(String.format("Current time: %02d.%02d.%02d", currHour, currMin, currSec));
        if (currSec >= 60) {
            // Add 1 minute and subtract 60 seconds.
            incrementTime(100 - 60 );
        }
        if (currMin >= 60) {
            // Add one hour (100*100 = 1:00:00) and subtract 60 minutes (60 * 100 = 60:00).
            incrementTime((100 - 60) * 100);
        }
        if (currHour >= 24) {
            // If the clock goes from 23:59:59 to 24:00:00, we set it to 00:00:00.
            setTime(0);
        }
    }

    private int getCurrentHour() throws InterruptedException {
        mutex.acquire();
        var hour = this.time / 10000;
        mutex.release();
        return hour;
    }

    private int getCurrentMin() throws InterruptedException {
        mutex.acquire();
        var min = (this.time % 10000 )/ 100;
        mutex.release();
        return min;
    }

    private int getCurrentSec() throws InterruptedException {
        mutex.acquire();
        var sec = this.time % 100;
        mutex.release();
        return sec;
    }

// ------ ALARM TIME ------
    public int getAlarmTime() throws InterruptedException {
        mutex.acquire();
        var tmp = alarmTime;
        mutex.release();
        return tmp;
    }

    public void setAlarmTime(int alarmTime) throws InterruptedException {
        mutex.acquire();
        this.alarmTime = alarmTime;
        mutex.release();
    }

// ------- ALARM STATE -------

    public boolean alarmShouldRing() throws InterruptedException {
        mutex.acquire();
        var aBoolean = alarmActive && time == alarmTime;
        mutex.release();
        return aBoolean;
    }

    public boolean isAlarmActive() throws InterruptedException {
        mutex.acquire();
        var tmp = alarmActive;
        mutex.release();
        return tmp;
    }

    public void setAlarmActive(boolean alarmActive) throws InterruptedException {
        mutex.acquire();
        this.alarmActive = alarmActive;
        mutex.release();
    }

    public void toggleAlarm() throws InterruptedException {
        setAlarmActive(!isAlarmActive());
    }

    public boolean alarmShouldShutUp() throws InterruptedException {
        mutex.acquire();
        var a = alarmShouldShutUp;
        mutex.release();
        return a;
    }

    public void setAlarmShouldShutUp(boolean alarmShouldShutUp) throws InterruptedException {
        mutex.acquire();
        this.alarmShouldShutUp = alarmShouldShutUp;
        mutex.release();
    }
}
