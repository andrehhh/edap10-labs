import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;

import clock.ClockInput;
import clock.ClockInput.UserInput;
import clock.ClockOutput;
import emulator.AlarmClockEmulator;

public class ClockMain {

    public static void main(String[] args) throws InterruptedException {
        AlarmClockEmulator emulator = new AlarmClockEmulator();

        ClockInput  in  = emulator.getInput();
        ClockOutput out = emulator.getOutput();

        Semaphore input = in.getSemaphore();
        Semaphore data = new Semaphore(1);

        Data clockData = new Data(5955, data);

        // TODO tmp, just to not have to press buttons when testing.
        clockData.setAlarmTime(10000);
        clockData.setAlarmActive(true);
        out.setAlarmIndicator(true);

        /* CLOCK THREAD. */
        new Thread( () -> {
            // one iteration in this loop should be pretty close to 1000ms.
            long startTime = System.currentTimeMillis();
            while (true) {
                try {
                    clockData.incrementTime(1);
                    var currentTime = clockData.getTime();
                    var alarmTime = clockData.getAlarmTime();
                    out.displayTime(currentTime);

                    if (clockData.isAlarmActive() && currentTime == alarmTime) {
                        clockData.setAlarmShouldShutUp(true);
                    }
                    if (currentTime == alarmTime+20) {
                        clockData.setAlarmShouldShutUp(false);
                    }
                    if (clockData.alarmShouldShutUp()) {
                        out.alarm();
                    }

                    long now = System.currentTimeMillis();
                    long drag = (now - startTime) % 1000;

                    Thread.sleep(1000 - drag);
                    System.out.println("Drag: " + drag);
                } catch (Exception ignored) {}
            }
        }, "clock").start();

        /* MAIN THREAD. */
        while (true) {
            input.acquire(); // wait for user input

            UserInput userInput = in.getUserInput(); // getUserInput releases the input semaphore.
            int choice = userInput.getChoice();
            int value = userInput.getValue();

            System.out.println("choice = " + choice + "  value=" + value);
            switch (choice) {
                case ClockInput.CHOICE_SET_TIME:
                    clockData.setTime(value);
                    break;
                case ClockInput.CHOICE_SET_ALARM:
                    clockData.setAlarmTime(value);
                    break;
                case ClockInput.CHOICE_TOGGLE_ALARM:
                    clockData.toggleAlarm();
                    out.setAlarmIndicator(clockData.isAlarmActive());
                    break;
            }
            // Alarm is always deactivated (if ringing) on input.
            clockData.setAlarmShouldShutUp(false);
        }
    }
}
